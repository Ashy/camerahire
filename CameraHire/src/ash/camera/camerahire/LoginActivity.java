package ash.camera.camerahire;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.GINGERBREAD) public class LoginActivity extends ActionBarActivity {


	
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_login);
	        
	       /*
	        * 
	        * 
	        *   Button btnregisterUser = (Button) findViewById(R.id.btnRegisterMe);
	        	btnregisterUser.setOnClickListener(new OnClickListener(){
	        	EditText txtEmailAddress = (EditText)findViewById(R.id.txtEmailAddress);
	        */
	        
	        Button btnUserLogin = (Button) findViewById(R.id.btnUserLogin);
	        btnUserLogin.setOnClickListener(new OnClickListener(){
	        	
	        	public void onClick (View V){
	        		
	        		TextView ErrorText = (TextView)findViewById(R.id.textView3);
	        		
	        		//textview3
	        		
	        		
	        		//Grab Entered Username in textbox
	        		EditText txtLoginUsername = (EditText)findViewById(R.id.txtLoginUsername);
	        		String LoginUsername = txtLoginUsername.getText().toString();
	        		
	        		//Grab Entered Username in textbox
	        		EditText txtLoginPassword = (EditText)findViewById(R.id.txtLoginPassword);
	        		String LoginPassword = txtLoginPassword.getText().toString();
	        		
	        		
	        		if(LoginUsername.isEmpty() || LoginPassword.isEmpty()){
	        			ErrorText.setText("Username and Password Required");
	        		}else{
	        			
	        			try{
 					       JSONObject json = null;
 					       String str = "";
 					       HttpResponse response;
 					       HttpClient myClient = new DefaultHttpClient();
 					       HttpPost myConnection = new HttpPost("http://ashspire.com/webservice/login.php?username="+LoginUsername+"&password="+LoginPassword+"");
 					       response = myClient.execute(myConnection);
 					       str = EntityUtils.toString(response.getEntity(), "UTF-8");
 					       JSONArray jArray = new JSONArray(str);
 					       json = jArray.getJSONObject(0);
 					       
 					       String Message = (json.getString("Message"));
 					       ErrorText.setText(Message);

 					       
 				    }catch(Exception e ){
 				    	e.getStackTrace();
 				    	ErrorText.setText("Data Did not Send :(");
 				    }
	        			
	        			
	        			//ErrorText.setText("All is ok for now");
	        		}
	        		
	        		
	        		
	        		
	        	}
	        	
	        });
	        
	        
	        
	        

	 }

	 
}